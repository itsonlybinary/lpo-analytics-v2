package eu.letsplayonline.analytics.core.util;

import java.util.UUID;

/**
 * This is the analytics-core representation of a player.
 * 
 * @author Johannes Frank
 * 
 */
public class Entity {

	private final String name;
	private final UUID id;
	private final String host;
	private final String locationname;
	private final EntityType type;
	private final String clientVersion;

	public Entity(UUID id, String name, String host, String locationname,
			String clientVersion, EntityType type) {
		this.name = name;
		this.id = id;
		this.locationname = locationname;
		this.type = type;
		this.host = host;
		this.clientVersion = clientVersion;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the client version.
	 */
	public String getClientVersion() {
		return clientVersion;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @return the locationname
	 */
	public String getLocationName() {
		return locationname;
	}

	/**
	 * Returns the locationn ame as URL part for the Server adress call from
	 * Analytics
	 * 
	 * @return an url-formatted representation of the player's location.
	 */
	public String getLocationNameAsUrl() {
		return "/server/" + getLocationName();
	}

	/**
	 * Returns the location name as augmented string for Analytics.
	 * 
	 * @return an augmented representation of the player's location.
	 */
	public String getLocationNameAugmented() {
		return "Server - " + getLocationName();
	}

	/**
	 * @return the type
	 */
	public EntityType getType() {
		return type;
	}
}
