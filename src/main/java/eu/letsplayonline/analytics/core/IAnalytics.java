package eu.letsplayonline.analytics.core;

import eu.letsplayonline.analytics.core.util.Entity;

/**
 * This is the interface to our Analytics framework. Every API call MUST be
 * defined here, all bridges MUST only use this interface for reference to the
 * analytics core framework. This allows us for future decoupling.
 * 
 * @author Johannes Frank
 * 
 */
public interface IAnalytics {

	/**
	 * This method should be called whenever a player joins.
	 * 
	 * @param entity
	 *            - the player that joins.
	 */
	public void join(Entity entity);

	/**
	 * This method should be called whenever a player leaves by choice.
	 * 
	 * @param entity
	 *            - the player that leaves.
	 */
	public void leave(Entity entity);

	/**
	 * This method should be called whenever a player leaves by force.
	 * 
	 * @param entity
	 *            - the player that was kicked.
	 * @param reason
	 *            - why the player was kicked.
	 */
	public void kick(Entity entity, String reason);

	/**
	 * This method should be called whenever a player says something publicly.
	 * 
	 * @param entity
	 *            - the player that talks.
	 * @param text
	 *            - what the player said.
	 * @param channel
     *            - where the player said it.
	 */
	public void talk(Entity entity, String text, String channel);

	/**
	 * This method should be called whenever a player whispers something to
	 * another player.
	 * 
	 * @param entity
	 *            - The player that whispers.
	 * @param to
	 *            - Whom he whispers to.
	 */
	public void whisper(Entity entity, String to);

	/**
	 * This method should be called whenever a player dies.
	 * 
	 * @param entity
	 *            - The player that dieded.
	 * @param cause
	 *            - Why the player died.
	 */
	public void die(Entity entity, String cause);

	/**
	 * This method should be called whenever a player changes its server.
	 * 
	 * @param entity
	 *            - The player that changed the server.
	 * @param targetServer
	 *            - Which server he changed to.
	 */
	public void changeServer(Entity entity, String targetServer);

	/**
	 * This method should be called whenever a player changes its world on a
	 * single server.
	 * 
	 * @param entity
	 *            - The player that changed the world.
	 * @param targetWorld
	 *            - Which world he changed to.
	 */
	public void changeWorld(Entity entity, String targetWorld);

	/**
	 * This method should be called whenever a player issues a command.
	 * 
	 * @param entity
	 *            - The player that issues the command.
	 * @param command
	 *            - Which command the player issued.
	 */
	public void command(Entity entity, String command);
}
