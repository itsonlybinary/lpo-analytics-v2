package eu.letsplayonline.analytics.core;

import java.util.NoSuchElementException;

import eu.letsplayonline.analytics.core.internal.AnalyticsImpl;

/**
 * This class serves as the entry point to the lpo analytics framework.
 * 
 * @author Johannes Frank
 * 
 */
public class Analytics {

	/**
	 * holds the google analytics tracing Id.
	 */
	private static String trackerId = null;

	/**
	 * Holds the hostname of this sevrer.
	 */
	private static String hostname = null;

	/**
	 * Instance variable for the analytics object.
	 */
	private static IAnalytics instance = null;

	/**
	 * Locker object used for synchronization.
	 */
	private static final Object lock = new Object();

	/**
	 * This method is used to configure the framework.
	 * 
	 * @param hostname
	 *            - the host name to tie the events to.
	 * @param trackerId
	 *            - the tracker ID to use for google API calls.
	 */
	public static void configure(String hostname, String trackerId) {
		Analytics.hostname = hostname;
		Analytics.trackerId = trackerId;
	}

	/**
	 * Starts the analytics framework.
	 * 
	 * @throws IllegalStateException
	 *             - When the framework is already running.
	 * @throws NoSuchElementException
	 *             - When no configuration was given before starting.
	 */
	public static void start() throws IllegalStateException,
			NoSuchElementException {
		synchronized (lock) {
			if (instance == null) {
				if (hostname != null && trackerId != null) {
					instance = new AnalyticsImpl(hostname, trackerId);
				} else {
					throw new NoSuchElementException(
							"Either hostname or trackerId were not given properly!");
				}
			} else {
				throw new IllegalStateException(
						"Could not start analytics framework, already started!");
			}
		}
	}

	/**
	 * Stops the analytics framework.
	 * 
	 * @throws IllegalStateException
	 *             - When the framework isn't running.
	 */
	public static void stop() throws IllegalStateException {
		synchronized (lock) {
			if (instance != null) {
				((AnalyticsImpl) instance).shutdown();
				instance = null;
			} else {
				throw new IllegalStateException(
						"Could not stop analytics framework, wasn't running!");
			}
		}
	}

	/**
	 * Returns an instance of the analytics implementation.
	 * 
	 * @return - an instance of the analytics implementation. Multiple calls
	 *         will return the same instance, unless stop is called.
	 * @throws IllegalStateException
	 *             - When the framework isn't running.
	 */
	public static IAnalytics getInstance() throws IllegalStateException {
		synchronized (lock) {
			if (instance != null) {
				return instance;
			} else {
				throw new IllegalStateException(
						"Could not get instance, analytics must be explicitly started with Analytics.start() first!");
			}
		}
	}
}
