package eu.letsplayonline.analytics.core.internal.events;

import eu.letsplayonline.analytics.core.internal.AnalyticsImpl;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.Values;

/**
 * This event is used to cause pageview hits. This can be used both to track
 * player on-time as well as server switches.
 * 
 * @author jfrank
 * 
 */
public class OnlineEvent extends EventBase {

	public OnlineEvent(Entity entity, AnalyticsImpl base) {
		super(entity, base);
		addParam(Values.HIT_TYPE, "pageview");
	}

}
