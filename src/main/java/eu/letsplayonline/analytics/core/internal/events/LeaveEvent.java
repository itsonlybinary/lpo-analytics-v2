package eu.letsplayonline.analytics.core.internal.events;

import eu.letsplayonline.analytics.core.internal.AnalyticsImpl;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.Values;

/**
 * This event handles when a player leaves the server (network).
 * 
 * @author Johannes Frank
 * 
 */
public class LeaveEvent extends EventBase {

	/**
	 * Constructor of the leave event
	 * 
	 * @param whoJoined
	 *            - the player that left.
	 * @param graceful
	 *            - whether or not he left by himself (graceful, true) or was
	 *            kicked (not graceful, false).
	 * @param reason
	 *            - if he was kicked, why?
	 * @param base
	 *            - the AnalyticsImpl instance.
	 */
	public LeaveEvent(Entity whoJoined, boolean graceful, String reason,
			AnalyticsImpl base) {
		super(whoJoined, base);
		addParam(Values.HIT_TYPE, "event");
		addParam(Values.EVENT_CATEGORY, "Player Connection");
		addParam(Values.SESSION_CONTROL, "end");
		if (graceful) {
			addParam(Values.EVENT_ACTION, "Quit");
			addParam(Values.EVENT_LABEL, "Player quit");
		} else {
			addParam(Values.EVENT_ACTION, "Kick");
			addParam(Values.EVENT_LABEL, "Player Kicked - " + reason);
		}
	}
	
	@Override
	public void run() {
		super.run();
	}
}
