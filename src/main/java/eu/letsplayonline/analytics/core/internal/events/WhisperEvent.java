/**
 * 
 */
package eu.letsplayonline.analytics.core.internal.events;

import eu.letsplayonline.analytics.core.internal.AnalyticsImpl;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.Values;

/**
 * This class handles events when a player says something privately.
 * 
 * @author Johannes Frank
 * 
 */
public class WhisperEvent extends EventBase {

	public WhisperEvent(Entity entity, String recepient, AnalyticsImpl base) {
		super(entity, base);
		addParam(Values.HIT_TYPE, "event");
		addParam(Values.EVENT_CATEGORY, "Player Whisper");
		addParam(Values.EVENT_ACTION, recepient);
		addParam(Values.EVENT_LABEL, "Private message");
	}

}
