package eu.letsplayonline.analytics.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.NoSuchElementException;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.letsplayonline.analytics.core.Analytics;
import eu.letsplayonline.analytics.core.internal.AnalyticsImpl;
import eu.letsplayonline.analytics.core.internal.events.CommandEvent;
import eu.letsplayonline.analytics.core.internal.events.DieEvent;
import eu.letsplayonline.analytics.core.internal.events.EventBase;
import eu.letsplayonline.analytics.core.internal.events.JoinEvent;
import eu.letsplayonline.analytics.core.internal.events.LeaveEvent;
import eu.letsplayonline.analytics.core.internal.events.OnlineEvent;
import eu.letsplayonline.analytics.core.internal.events.TalkEvent;
import eu.letsplayonline.analytics.core.internal.events.WhisperEvent;
import eu.letsplayonline.analytics.core.util.Entity;
import eu.letsplayonline.analytics.core.util.EntityType;

/**
 * This class contains the tests for the Analytics Core framework. It uses
 * Powermock to mock the URL Call so that we can analyze where the call would
 * have gone without causing actual traffic.
 * 
 * @author Johannes Frank
 * 
 */
@RunWith(PowerMockRunner.class)
public class TestAnalyticsFramework {

	/**
	 * Tests whether or not the state changes are all working as intended.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testStateChanges() throws Exception {

		try {
			Analytics.stop();
		} catch (IllegalStateException e) {
			// Should throw an Exception, not startet yet.
			assertTrue("Stopping a stopped analytics threw exception", true);
		}

		try {
			Analytics.getInstance();
		} catch (IllegalStateException e) {
			// Should throw an exception, not started yet.
			assertTrue(
					"Trying to get an instance from a stopped analytics threw exception",
					true);
		}
		try {
			Analytics.start();
		} catch (NoSuchElementException e) {
			assertTrue(
					"Got exception because analytics are not correctly configured",
					true);
		}
		Analytics.configure("localhost", "lalala");
		Analytics.start();
		assertTrue("Started without exception", true);
		try {
			Analytics.start();
		} catch (IllegalStateException e) {
			// Should throw an exception, already started.
			assertTrue("Starting a started analytics threw exception", true);
		}
		Analytics.getInstance();
		assertTrue("Successfully got instance from started analytics", true);

		Analytics.stop();
		assertTrue("Successfully stopped analytics", true);
	}

	/**
	 * Tests the entity util class.
	 */
	@Test
	public void testEntity() {
		UUID id = UUID.randomUUID();
		String name = "Testname";
		String hostname = "Testhost";
		String playerhost = "Playerhost";
		String version = "1.6.4";
		EntityType type = EntityType.PLAYER;
		Entity entity = new Entity(id, name, playerhost, hostname, version,
				type);
		assertEquals("Checking id", id, entity.getId());
		assertEquals("Checking name", name, entity.getName());
		assertEquals("Checking hostname", hostname, entity.getLocationName());
		assertEquals("Checking hostname url representation", "/server/"
				+ hostname, entity.getLocationNameAsUrl());
		assertEquals("Checking hostname augmented", "Server - " + hostname,
				entity.getLocationNameAugmented());
		assertEquals("Checking type", type, entity.getType());
		assertEquals("Checking player host", playerhost, entity.getHost());
		assertEquals("Checking client version", version,
				entity.getClientVersion());
	}

	/**
	 * Tests the payload generation of the join event.
	 * 
	 * @throws Exception
	 *             - should not :)
	 */
	@Test
	public void testJoinEvent() throws Exception {
		AnalyticsImpl base = new AnalyticsImpl("testhost", "testId");
		JoinEvent je = new JoinEvent(new Entity(
				UUID.fromString("6e8c52f1-2ae8-4a07-90d8-15e30b79dab0"),
				"TestPlayer", "localhost", "Testserver", "1.6.4",
				EntityType.PLAYER), base);
		Method method = je.getClass().getSuperclass()
				.getDeclaredMethod("serializeParameters");
		method.setAccessible(true);
		String result = (String) method.invoke(je, new Object[] {});
		assertEquals(
				"Checking if payload matches saved value",
				result,
				"dt=Server+-+Testserver&sc=start&t=event&v=1&fl=1.6.4&el=Player+Join&dl=http%3A%2F%2Ftesthost%2Fserver%2FTestserver&ea=Join&uip=localhost&tid=testId&ec=Player+Connection&cid=6e8c52f1-2ae8-4a07-90d8-15e30b79dab0");
	}

	/**
	 * Tests the payload generation of the graceful leave event.
	 * 
	 * @throws Exception
	 *             - should not :)
	 */
	@Test
	public void testGracefuLeaveEvent() throws Exception {
		AnalyticsImpl base = new AnalyticsImpl("testhost", "testId");
		LeaveEvent je = new LeaveEvent(new Entity(
				UUID.fromString("6e8c52f1-2ae8-4a07-90d8-15e30b79dab0"),
				"TestPlayer", "localhost", "Testserver", "1.6.4",
				EntityType.PLAYER), true, null, base);
		Method method = je.getClass().getSuperclass()
				.getDeclaredMethod("serializeParameters");
		method.setAccessible(true);
		String result = (String) method.invoke(je, new Object[] {});
		assertEquals(
				"Checking if payload matches saved value",
				result,
				"dt=Server+-+Testserver&sc=end&t=event&v=1&fl=1.6.4&el=Player+quit&dl=http%3A%2F%2Ftesthost%2Fserver%2FTestserver&ea=Quit&uip=localhost&tid=testId&ec=Player+Connection&cid=6e8c52f1-2ae8-4a07-90d8-15e30b79dab0");
	}

	/**
	 * Tests the payload generation of the not graceful leave event.
	 * 
	 * @throws Exception
	 *             - should not :)
	 */
	@Test
	public void testKickLeaveEvent() throws Exception {
		AnalyticsImpl base = new AnalyticsImpl("testhost", "testId");
		LeaveEvent je = new LeaveEvent(new Entity(
				UUID.fromString("6e8c52f1-2ae8-4a07-90d8-15e30b79dab0"),
				"TestPlayer", "localhost", "Testserver", "1.6.4",
				EntityType.PLAYER), false, "Inproper behavior", base);
		Method method = je.getClass().getSuperclass()
				.getDeclaredMethod("serializeParameters");
		method.setAccessible(true);
		String result = (String) method.invoke(je, new Object[] {});
		assertEquals(
				"Checking if payload matches saved value",
				result,
				"dt=Server+-+Testserver&sc=end&t=event&v=1&fl=1.6.4&el=Player+Kicked+-+Inproper+behavior&dl=http%3A%2F%2Ftesthost%2Fserver%2FTestserver&ea=Kick&uip=localhost&tid=testId&ec=Player+Connection&cid=6e8c52f1-2ae8-4a07-90d8-15e30b79dab0");
	}

	/**
	 * Tests the payload generation of the online pageview event.
	 * 
	 * @throws Exception
	 *             - should not :)
	 */
	@Test
	public void testOnlineEvent() throws Exception {
		AnalyticsImpl base = new AnalyticsImpl("testhost", "testId");
		OnlineEvent je = new OnlineEvent(new Entity(
				UUID.fromString("6e8c52f1-2ae8-4a07-90d8-15e30b79dab0"),
				"TestPlayer", "localhost", "Testserver", "1.6.4",
				EntityType.PLAYER), base);
		Method method = je.getClass().getSuperclass()
				.getDeclaredMethod("serializeParameters");
		method.setAccessible(true);
		String result = (String) method.invoke(je, new Object[] {});
		assertEquals(
				"Checking if payload matches saved value",
				result,
				"dt=Server+-+Testserver&t=pageview&v=1&fl=1.6.4&dl=http%3A%2F%2Ftesthost%2Fserver%2FTestserver&uip=localhost&tid=testId&cid=6e8c52f1-2ae8-4a07-90d8-15e30b79dab0");
	}

	/**
	 * Tests the payload generation of the Talk event.
	 * 
	 * @throws Exception
	 *             - should not :)
	 */
	@Test
	public void testTalkEvent() throws Exception {
		AnalyticsImpl base = new AnalyticsImpl("testhost", "testId");
		EventBase je = new TalkEvent(new Entity(
				UUID.fromString("6e8c52f1-2ae8-4a07-90d8-15e30b79dab0"),
				"TestPlayer", "localhost", "Testserver", "1.6.4",
				EntityType.PLAYER), "someText", "theChannel", base);
		Method method = je.getClass().getSuperclass()
				.getDeclaredMethod("serializeParameters");
		method.setAccessible(true);
		String result = (String) method.invoke(je, new Object[] {});
		assertEquals(
				"Checking if payload matches saved value",
				result,
				"dt=Server+-+Testserver&t=event&v=1&fl=1.6.4&el=Chat+message&dl=http%3A%2F%2Ftesthost%2Fserver%2FTestserver&ea=theChannel&uip=localhost&tid=testId&ec=Player+Chat&cid=6e8c52f1-2ae8-4a07-90d8-15e30b79dab0");
	}

	/**
	 * Tests the payload generation of the Whisper event.
	 * 
	 * @throws Exception
	 *             - should not :)
	 */
	@Test
	public void testWhisperEvent() throws Exception {
		AnalyticsImpl base = new AnalyticsImpl("testhost", "testId");
		EventBase je = new WhisperEvent(new Entity(
				UUID.fromString("6e8c52f1-2ae8-4a07-90d8-15e30b79dab0"),
				"TestPlayer", "localhost", "Testserver", "1.6.4",
				EntityType.PLAYER), "targetPlayer", base);
		Method method = je.getClass().getSuperclass()
				.getDeclaredMethod("serializeParameters");
		method.setAccessible(true);
		String result = (String) method.invoke(je, new Object[] {});
		assertEquals(
				"Checking if payload matches saved value",
				result,
				"dt=Server+-+Testserver&t=event&v=1&fl=1.6.4&el=Private+message&dl=http%3A%2F%2Ftesthost%2Fserver%2FTestserver&ea=targetPlayer&uip=localhost&tid=testId&ec=Player+Whisper&cid=6e8c52f1-2ae8-4a07-90d8-15e30b79dab0");
	}

	/**
	 * Tests the payload generation of the Death event.
	 * 
	 * @throws Exception
	 *             - should not :)
	 */
	@Test
	public void testDieEvent() throws Exception {
		AnalyticsImpl base = new AnalyticsImpl("testhost", "testId");
		EventBase je = new DieEvent(new Entity(
				UUID.fromString("6e8c52f1-2ae8-4a07-90d8-15e30b79dab0"),
				"TestPlayer", "localhost", "Testserver", "1.6.4",
				EntityType.PLAYER), "By natrual cause", base);
		Method method = je.getClass().getSuperclass()
				.getDeclaredMethod("serializeParameters");
		method.setAccessible(true);
		String result = (String) method.invoke(je, new Object[] {});
		assertEquals(
				"Checking if payload matches saved value",
				result,
				"dt=Server+-+Testserver&t=event&v=1&fl=1.6.4&el=Player+died&dl=http%3A%2F%2Ftesthost%2Fserver%2FTestserver&ea=By+natrual+cause&uip=localhost&tid=testId&ec=Player+Death&cid=6e8c52f1-2ae8-4a07-90d8-15e30b79dab0");
	}

	/**
	 * Tests the payload generation of the Command event.
	 * 
	 * @throws Exception
	 *             - should not :)
	 */
	@Test
	public void testCommandEvent() throws Exception {
		AnalyticsImpl base = new AnalyticsImpl("testhost", "testId");
		EventBase je = new CommandEvent(new Entity(
				UUID.fromString("6e8c52f1-2ae8-4a07-90d8-15e30b79dab0"),
				"TestPlayer", "localhost", "Testserver", "1.6.4",
				EntityType.PLAYER), "sethome", base);
		Method method = je.getClass().getSuperclass()
				.getDeclaredMethod("serializeParameters");
		method.setAccessible(true);
		String result = (String) method.invoke(je, new Object[] {});
		assertEquals(
				"Checking if payload matches saved value",
				result,
				"dt=Server+-+Testserver&t=event&v=1&fl=1.6.4&el=Player+Command&dl=http%3A%2F%2Ftesthost%2Fserver%2FTestserver&ea=sethome&uip=localhost&tid=testId&ec=Player+Command&cid=6e8c52f1-2ae8-4a07-90d8-15e30b79dab0");
	}
}
